package com.godwin.serialinterface.worker

import android.os.Process
import androidx.annotation.NonNull

import java.util.concurrent.ThreadFactory


/**
 * Created by Godwin on 03-11-2017 11:47 for WiSeItAndroid.
 *
 * @author : Godwin Joseph Kurinjikattu
 */

class PriorityThreadFactory internal constructor(private val mThreadPriority: Int) : ThreadFactory {

    override fun newThread(@NonNull runnable: Runnable): Thread {
        val wrapperRunnable = {
            try {
                Process.setThreadPriority(mThreadPriority)
            } catch (ignored: Throwable) {
            }

            runnable.run()
        }
        return Thread(wrapperRunnable)
    }

}
