package com.godwin.serialinterface.worker

import android.os.Process
import androidx.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Created by WiSilica on 3/4/2019 4:56 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
object ThreadPoolManager {
    val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()
    private var mProvider: ThreadPoolManager? = null
    /*
     * thread pool executor for background tasks
     */
    private var mForBackgroundTasks: ThreadPoolExecutor
    /*
     * thread pool executor for main thread tasks
     */
    private var mMainThreadExecutor: Executor

    init{
        //restricts initialization

        // setting the thread factory
        val backgroundPriorityThreadFactory = PriorityThreadFactory(Process.THREAD_PRIORITY_BACKGROUND)

        // setting the thread pool executor for mForBackgroundTasks;
        mForBackgroundTasks = ThreadPoolExecutor(
            NUMBER_OF_CORES * 2,
            NUMBER_OF_CORES * 2,
            60L,
            TimeUnit.SECONDS,
            LinkedBlockingQueue(),
            backgroundPriorityThreadFactory
        )
        // setting the thread pool executor for mMainThreadExecutor;
        mMainThreadExecutor = MainThreadExecutor()
    }

    /**
     * Execute on main thread.
     *
     * @param runnable the runnable
     */
    fun executeOnMainThread(@NonNull runnable: Runnable) {
        mMainThreadExecutor.execute(runnable)
    }

    /**
     * Execute back ground task.
     *
     * @param runnable the runnable
     */
    fun executeBackGroundTask(@NonNull runnable: Runnable) {
        mForBackgroundTasks.execute(runnable)
    }
}