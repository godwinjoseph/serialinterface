package com.godwin.serialinterface.worker

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull

import java.util.concurrent.Executor

/**
 * Created by Godwin on 03-11-2017 11:46 for WiSeItAndroid.
 *
 * @author : Godwin Joseph Kurinjikattu
 */

class MainThreadExecutor : Executor {

    private val handler = Handler(Looper.getMainLooper())

    override fun execute(@NonNull runnable: Runnable) {
        handler.post(runnable)
    }
}
