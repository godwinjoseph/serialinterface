package com.godwin.serialinterface.connection

import com.godwin.serialinterface.worker.ThreadPoolManager

/**
 * Created by WiSilica on 2019-03-05 15:01.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017:
 */
object TcpConnectionManager {
    private  var tcpServer: TcpServerManager?=null
    private  var tcpClient: TcpClientManager?=null

    fun startServer(port: Int) {
        tcpServer = TcpServerManager(port)
        ThreadPoolManager.executeBackGroundTask(tcpServer!!)
    }

    fun stopServer() {
        if (tcpServer != null) {
            tcpServer!!.stopServer()
        }
    }

    fun startClient(ip: String, port: Int) {
        tcpClient = TcpClientManager(ip, port)
        ThreadPoolManager.executeBackGroundTask(tcpClient!!)
    }

    fun stopClient() {
        if (tcpClient != null) {
            tcpClient!!.stopClient()
        }
    }
}