package com.godwin.serialinterface.connection

import com.godwin.serialinterface.util.LogcatLogger
import com.godwin.serialinterface.worker.ThreadPoolManager
import java.net.ServerSocket
import java.net.Socket

/**
 * Created by WiSilica on 2019-03-05 15:13.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017:
 */
class TcpClientManager(private val ip: String, private val port: Int) : Runnable {
    private var socket: Socket?=null
    override fun run() {
        startTcpClient()
    }

    fun stopClient() {
        if (socket != null) {
            socket!!.close()
            socket=null
        }
    }

    private fun startTcpClient() {
        socket = Socket(ip, port)
        LogcatLogger.i(this.javaClass.simpleName, String.format("Started status "))
        val handler = ClientConnectionHandler(socket!!)
        ThreadPoolManager.executeBackGroundTask(handler)
        TcpCallbackSubscriber.publishOnConnected(socket!!)
    }
}