package com.godwin.serialinterface.connection

import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.ServerSocket
import java.net.Socket


/**
 * Created by WiSilica on 3/4/2019 4:40 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
class ServerConnectionHandler(val socket: Socket, val serverSocket: ServerSocket) : Runnable {
    override fun run() {
        val inStream = DataInputStream(socket.getInputStream())
        val outStream = DataOutputStream(socket.getOutputStream())
        var clientMessage = ""
        while (clientMessage == "**//**") {
            clientMessage = inStream.readUTF()
            println(clientMessage)

            TcpCallbackSubscriber.publishOnMessage(socket, clientMessage)

            outStream.writeUTF("$clientMessage*")
            outStream.flush()
        }
        inStream.close()
        outStream.close()
        socket.close()

        TcpCallbackSubscriber.publishOnClosed(socket)
    }
}
