package com.godwin.serialinterface.connection

import com.godwin.serialinterface.util.LogcatLogger
import com.godwin.serialinterface.worker.ThreadPoolManager
import java.net.ServerSocket
import java.net.Socket
import android.text.format.Formatter.formatIpAddress
import android.content.Context.WIFI_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager
import android.text.format.Formatter
import java.net.InetSocketAddress


/**
 * Created by WiSilica on 3/4/2019 2:09 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
class TcpServerManager(private val port: Int) : Runnable {
    private var serverSocket: ServerSocket? = null
    fun stopServer() {
        if (serverSocket != null) {
            serverSocket!!.close()
            serverSocket = null
        }
    }

    override fun run() {
        startTcpServer()
    }

    var connectionList: ArrayList<Socket> = ArrayList(1)
    var isAvailable = true

    private fun startTcpServer() {

        serverSocket = ServerSocket()
        serverSocket!!.reuseAddress = true
        serverSocket!!.bind(InetSocketAddress(port))
        LogcatLogger.i(this.javaClass.simpleName, String.format("Started status %b", isAvailable))
        while (isAvailable) {
            val socket = serverSocket!!.accept()
            addConnection(socket, serverSocket!!)
            TcpCallbackSubscriber.publishOnConnected(socket)

            val handler = ServerConnectionHandler(socket, serverSocket!!)
            ThreadPoolManager.executeBackGroundTask(handler)
        }
    }

    fun setAvailability(isAvailable: Boolean) {
        this.isAvailable = isAvailable
    }

    private fun addConnection(socket: Socket, serverSocket: ServerSocket) {
        if (!connectionList.contains(socket)) {
            connectionList.add(socket)
        }
    }

    private fun removeConnection(socket: Socket) {
        try {
            socket.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun removeAllConnection() {
        for (socket in connectionList) {
            try {
                socket.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}