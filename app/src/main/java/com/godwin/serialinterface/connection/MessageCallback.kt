package com.godwin.serialinterface.connection

import java.net.Socket

/**
 * Created by WiSilica on 3/4/2019 3:49 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
interface MessageCallback {
    fun onClientConnected(socket: Socket)
    fun onMessageRecieved(socket: Socket, message: String)
    fun onClientCloed(socket: Socket)
}