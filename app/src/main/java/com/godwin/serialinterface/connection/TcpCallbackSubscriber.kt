package com.godwin.serialinterface.connection

import java.net.Socket

/**
 * Created by WiSilica on 3/4/2019 5:09 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
object TcpCallbackSubscriber {
    val callbackList = ArrayList<Any>()

    fun subscriber(any: Any) {
        if (!callbackList.contains(any)) {
            callbackList.add(any)
        }
    }

    fun unSubscribe(any: Any) {
        callbackList.remove(any)
    }

    fun publishOnConnected(socket: Socket) {
        for (any in callbackList) {
            if (any is MessageCallback) {
                any.onClientConnected(socket)
            }
        }
    }

    fun publishOnClosed(socket: Socket) {
        for (any in callbackList) {
            if (any is MessageCallback) {
                any.onClientCloed(socket)
            }
        }
    }

    fun publishOnMessage(socket: Socket, message: String) {
        for (any in callbackList) {
            if (any is MessageCallback) {
                any.onMessageRecieved(socket, message)
            }
        }
    }
}