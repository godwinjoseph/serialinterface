package com.godwin.serialinterface.util

import android.text.TextUtils
import android.util.Log

/**
 * Created by WiSilica on 3/4/2019 2:27 PM.
 *
 * @author : Godwin Joseph Kurinjikattu
 * @since : 2017
 */
public object LogcatLogger {
    private var C_TAG = "APP_TAG: "
    var VERBOSE_LOGGING = true

    /**
     * @param tag the tag
     * @param msg the msg
     */
    fun i(tag: String, msg: String) {
        if (VERBOSE_LOGGING && !TextUtils.isEmpty(msg)) {
            Log.i(formatTag(tag), msg)
        }
    }

    /**
     * V.
     *
     * @param tag the tag
     * @param msg the msg
     */
    fun v(tag: String, msg: String) {
        if (VERBOSE_LOGGING && !TextUtils.isEmpty(msg)) {
            Log.v(formatTag(tag), msg)
        }
    }

    /**
     * W.
     *
     * @param tag the tag
     * @param msg the msg
     */
    fun w(tag: String, msg: String) {
        if (VERBOSE_LOGGING && !TextUtils.isEmpty(msg)) {
            Log.w(formatTag(tag), msg)
        }
    }

    /**
     * D.
     *
     * @param tag the tag
     * @param msg the msg
     */
    fun d(tag: String, msg: String) {
        if (VERBOSE_LOGGING && !TextUtils.isEmpty(msg)) {
            Log.d(formatTag(tag), msg)
        }
    }

    /**
     * E.
     *
     * @param tag the tag
     * @param msg the msg
     */
    fun e(tag: String, msg: String) {
        if (VERBOSE_LOGGING && !TextUtils.isEmpty(msg)) {
            Log.e(formatTag(tag), msg)
        }
    }

    private fun formatTag(tag: String): String {
        // Tag names greater than 23 characters are truncated in the logcat output.
        return String.format("%s: %s", C_TAG, tag)
    }
}