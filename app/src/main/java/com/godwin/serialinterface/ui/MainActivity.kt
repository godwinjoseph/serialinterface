package com.godwin.serialinterface.ui

import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.godwin.serialinterface.R
import com.godwin.serialinterface.connection.TcpConnectionManager
import com.godwin.serialinterface.connection.TcpConnectionManager.stopServer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var isStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSpinner()

        groupClient.visibility = View.GONE
        groupServer.visibility = View.GONE
    }

    fun initSpinner() {
        ArrayAdapter.createFromResource(
            this,
            R.array.mode_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spMode.adapter = adapter
        }

        spMode.onItemSelectedListener = this
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        menu!!.findItem(R.id.action_start).isVisible = !isStarted
        menu.findItem(R.id.action_stop).isVisible = isStarted
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_start -> {
                start()
                isStarted = true
            }
            R.id.action_stop -> {
                stop()
                isStarted = false
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun start() {
        if (spMode.selectedItemPosition == 1) {
            //start server
            startServer()
            invalidateOptionsMenu()
        } else if (spMode.selectedItemPosition == 2) {
            //start client
            startClient()
            invalidateOptionsMenu()
        }
    }

    private fun stop() {
        if (spMode.selectedItemPosition == 1) {
            //start server
            TcpConnectionManager.stopServer()
            invalidateOptionsMenu()
        } else if (spMode.selectedItemPosition == 2) {
            //start client
            TcpConnectionManager.stopClient()
            invalidateOptionsMenu()
        }
    }

    private fun startServer() {
        val port = tilPort.editText!!.text.toString()
        if (TextUtils.isDigitsOnly(port)) {
            TcpConnectionManager.stopServer()
            TcpConnectionManager.startServer(port.toInt())
        }
    }

    private fun startClient() {
        val ip = tilPort.editText!!.text.toString()
        val port = tilPort.editText!!.text.toString()
        if (!TextUtils.isDigitsOnly(port) && TextUtils.isEmpty(ip)) {
            TcpConnectionManager.stopClient()
            TcpConnectionManager.startClient(ip, port.toInt())
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (position) {
            0 -> {
                groupClient.visibility = View.GONE
                groupServer.visibility = View.GONE
            }
            1 -> {
                groupClient.visibility = View.GONE
                groupServer.visibility = View.VISIBLE
            }
            else -> {
                groupClient.visibility = View.VISIBLE
                groupServer.visibility = View.VISIBLE
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }
}
